# Vagrant - MEAN stack with Yeoman generator (generator-angular-fullstack)

1. Install [Virtual Box](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html)

2. Provision the Vagrant box by running `vagrant up`

3. Log into the Vagrant box
  `vagrant ssh`
  `cd /vagrant`
  `npm install`
  `bower install`

4. Got to http://127.0.0.1:9000 on the host machine