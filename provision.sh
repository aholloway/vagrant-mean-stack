#!/usr/bin/env bash

#################################################################################
# Install and configure base
################################################################################

# Update packages list
sudo apt-get update

# Install base packages (Git, Vim, terminal multiplexer and repository manager)
sudo apt-get install -y unzip git-core ack-grep vim tmux curl wget build-essential python-software-properties

# Symlink /var/www to project web root
sudo rm -rf /var/www
sudo ln -fs /vagrant/public /var/www

# Set timezone
echo "Europe/London" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

# Set Vim as default editor
sudo update-alternatives --set editor /usr/bin/vim.basic

################################################################################
# Install generator-angular-fullstack and dependencies
################################################################################

# Get key and add to sources for MongoDB
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' > /etc/apt/sources.list.d/mongodb.list

# Update packages list
sudo apt-get update

# Install MongoDB
sudo apt-get -y install mongodb-10gen

# Add NodeJS repository
sudo add-apt-repository -y ppa:chris-lea/node.js

# Install generator-angular-fullstack
npm install -g yo generator-angular-fullstack

## Update Ruby gems and install compass
gem update --system && gem install compass

echo ">>> Finished installing MEAN stack"